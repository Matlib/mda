CONFIG_FILE=/etc/mda.conf

.POSIX:
CC?=gcc
CXX?=g++
CFLAGS=-s
CXXFLAGS=-Wall -O2 -fno-exceptions -Wno-misleading-indentation
O=*.o

MDA=bin/mda
MAN_MDA=man/man1/mda.1

VERSION=3

all: $(MDA) man
	@echo
	@echo "Build complete in bin/ and man/man1/"
	@echo
	@ls -Al bin/* man/man1/*
	@echo

help:
	@echo
	@echo "Available targets:"
	@echo "  all"
	@echo "  clean"
	@echo " " $(MDA)
	@echo " " $(MAN_MDA)
	@echo
	@echo "Defaults:"
	@echo "  CC       = $(CC)"
	@echo "  CXX      = $(CXX)"
	@echo "  CFLAGS   = $(CFLAGS)"
	@echo "  CXXFLAGS = $(CXXFLAGS)"
	@echo "  LDFLAGS  = $(LDFLAGS)"
	@echo
	@echo "  configuration file: $(CONFIG_FILE)"
	@echo

clean:
	rm -fr bin man

man: $(MAN_MDA)

$(MDA): src/*
	mkdir -p bin
	$(CXX) $(CXXFLAGS) -DVERSION=$(VERSION) -DCONFIG_FILE=$(CONFIG_FILE) -c src/*.cc
	$(CC) $(CFLAGS) -o $(MDA) $(LDFLAGS) $(O)
	rm $(O)

$(MAN_MDA): src/mda.man
	mkdir -p man/man1
	./mkman $(CONFIG_FILE) src/mda.man $(MAN_MDA)
