**mda** – relay only replacement for /usr/bin/sendmail

This program is a sendmail compatible replacement for the system mailer. It reads a message from standard input and forwards it to a predefined relay server. It does not handle mailboxes, not even for local delivery. It does not accept any mail from the outside world either.

If you need richer functionality (authentication, multiple personalities) you might want to try the [esmtp program](https://salsa.debian.org/carnil/esmtp).

# Compiling

A C++ compiler and make are needed to build the binary. Normally it is sufficient to execute `make`. This should compile the program in the `bin/` directory and the manual page in the `man/` directory.

The default compiler choice may be overriden with the *CC* and *CXX* parameters, i.e. `make CC=gcc CXX=g++`.

To change the location of the configuration file use the *CONFIG_FILE* parameter like `make CONFIG_FILE=/var/etc/mda.conf`.

# Examples

Send a simple message to multiple recipients:

```shell
( echo -n 'To: <alice@bobs.company>
Bcc: <archive@bobs.company>
Subject: New files '; date; echo; find . -mtime -1 ) | mda -ti
```

Send a text file setting the sender name as well as the subject. The return path
is empty inhibiting automatic replies.

```shell
mda -i -F 'Feliĉa Co.' -f '' -S 'Mangó Sales' < mango_report
```

# Author

Matt Latusek, matlib@matlibhax.com

GitLab project: https://gitlab.com/Matlib/mda
