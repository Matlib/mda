/*
   mda.cc
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
*/

#include "mda.hh"
#include "util.hh"
#include "smtp.hh"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <time.h>
#include <pwd.h>
#include <regex.h>

Recipient *recipients = nullptr, *recipients_last = nullptr;

void recipient_add (char const *const address)
{
	mail_check (address);
	Recipient *rcpt =
		 (Recipient *) emalloc (sizeof (Recipient) + strlen (address) + 1);
	strcpy (rcpt->data, address);
	if (recipients_last)
		recipients_last = recipients_last->next = rcpt;
	else
		recipients = recipients_last = rcpt;
}


char *from_local_user (char const *const base)
{
	static char *from = nullptr;
	if (from)
		return from;
	from = (char *) emalloc (1024);

	if (base && strlen (base))
	{
		if (strlen (base) > 600)
			error ("E-mail sender name '%s' too long", base);
		if (strchr (base, '@'))
		{
			strncpy (from, base, 1023);
			verbose (1, "Sender: %s", from);
			return from;
		}
		snprintf (from, 1023, "%s@%s", base, DOMAIN ? DOMAIN : host_fqdn ());
	}
	else
	{
		struct passwd *p;
		if (!(p = getpwuid (getuid ())))
			error ("Cannot get user info: %s", strerror (errno));
		snprintf (from, 1023, "%s@%s", p->pw_name, DOMAIN ? DOMAIN : host_fqdn ());
	}

	verbose (1, "Sender (autodetected): %s", from);
	if (strlen (from) > 600)
		error ("E-mail sender name '%s' too long", from);
	return from;
}

char const *config [4];

void config_read ()
{
	static char const *const config_keys [4] =
		 { "relay", "transmit log", "domain", "error mode" };
	memset (config, 0, sizeof (config));
	int fd = open (str (CONFIG_FILE), O_RDONLY | O_NOCTTY);
	if (fd == -1)
	{
		if (errno != ENOENT)
			fprintf (stderr, "Cannot open configuration file %s: %s\n",
				 str (CONFIG_FILE), strerror (errno));
		return;
	}

	FD cf (str (CONFIG_FILE), fd);
	cf.check_eof = true;
	Str line, key, value;
	regex_t *comment_empty = regex ("^[ \t]*(#.*|$)");
	regex_t *key_value = regex ("^[ \t]*([a-z ]+[a-z])[ \t]*=[ \t]*(.*)");

	unsigned l = 0, i;
	while (!cf.eof)
	{
		line = "";
		cf.readline (line);
		l++;
		if (line.find (0))
			error ("Configuration line %u contains nul character", l);
		if (regex_match (comment_empty, line))
			continue;
		if (!regex_match (key_value, line, 3, nullptr, &key, &value))
			error ("Invalid configuration line %u", l);
		i = 0;
		while (key != config_keys [i])
			if (++i == alen (config_keys))
				error ("Unknown configuration key '%s' at line %u", key.data, l);
		config [i] = estrdup (value);
	}

	regfree (comment_empty);
	regfree (key_value);
}

regex_t *mail_addr = nullptr;

void mail_check (char const *const address)
{
	if (!mail_addr)
		mail_addr = regex ("^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~.-]+(@[a-zA-Z0-9.-]+)?$");
	if (!regex_match (mail_addr, address) || strlen (address) > 200)
		error ("Invalid email address '%s'", address);
}

int trans_log_open (char const *const file, int const argc,
	 char const *const *const argv)
{
	int i;
	int trans_log = open (file, O_WRONLY | O_APPEND | O_CREAT | O_NOCTTY, 0666);
	if (trans_log == -1)
		error ("Cannot open transaction log '%s': %s", file, strerror (errno));
	Str log_header;
	log_header.strftime ("\n----%F %H:%M:%S----\n", time (nullptr));
	for (i = 0; i < argc; i++)
		log_header.printf ("%s%s", i ? " " : "", argv [i]);
	log_header += "\n\n";
	write (trans_log, log_header, log_header.length);
	return trans_log;
}

char const *args (int const argc, char const *const *const argv, int &argp,
	 char const *const error_msg)
{
	if (strlen (argv [argp]) > 2)
		return argv [argp] + 2;
	if (++argp == argc)
		error ("%s", error_msg);
	return argv [argp];
}

struct Option
{
	char shrt;
	char const *lng;
};

void argo (bool const lng, char const *const opt, char &name, Str &value)
{
	static Option names [2] =
	{
		{ 'e', "ErrorMode"  },
		{ 'i', "IgnoreDots" }
	};
	unsigned i = 0;

	if (!strlen (opt))
		error ("Option name and value expected after -o/-O");

	if (lng)
	{
		Str long_name;
		char const *v = strchr (opt, '=');
		if (!v)
			error ("Invalid option definition '%s'", opt);
		long_name.append (opt, (size_t) v - (size_t) opt);
		value = ++v;
		while (long_name != names [i].lng)
			if (++i == alen (names))
				error ("Unknown long option '%s'", opt);
		return;
	}

	name = opt [0];
	value = opt + 1;
	while (name != names [i].shrt)
		if (++i == alen (names))
			error ("Unknown short option '%s'", opt);
}

bool argb (Str &value)
{
	if (!value.length || value == "1" || value == "t" || value == "true")
		return true;
	if (value == "0" || value == "f" || value == "false")
		return false;
	error ("Invalid boolean value '%s'", value.data);
}

int main (int const argc, char const *const *const argv)
{
	int i;
	bool interactive = false, rcpt_headers = false;
	char const *from = nullptr;
	FD input ("stdin", STDIN_FILENO);
	Str full_name, subject;
	char *new_error_mode = nullptr;

	setenv ("LANG", "C", true);
	unsetenv ("LC_TIME");
	config_read ();
	if (ERROR_MODE)
		error_set_mode (ERROR_MODE);

	for (i = 1; i < argc; i++)
	{
		if (!strcmp (argv [i], "-i"))
			interactive = true;
		else if (!strcmp (argv [i], "-t"))
			rcpt_headers = true;
		else if (!strcmp (argv [i], "-ti"))
		{
			interactive = true;
			rcpt_headers = true;
		}
		else if (!strcmp (argv [i], "-f"))
			from = args (argc, argv, i, "Sender address expected after -f");
		else if (!strncmp (argv [i], "-F", 2))
		{
			full_name.base64 (args (argc, argv, i, "Sender name expected after -F"));
			if (strlen (full_name) > 600)
				error ("Maximum allowed sender name length is 400 bytes");
		}
		else if (!strncmp (argv [i], "-S", 2))
		{
			subject.base64 (args (argc, argv, i, "Subject expected after -S"));
			if (strlen (subject) > 600)
				error ("Maximum allowed subject length is 400 bytes");
		}
		else if (!strcmp (argv [i], "-G"))
			RELAY = args (argc, argv, i, "Gateway address expected after -G");
		else if (!strcmp (argv [i], "-Ac") || !strcmp (argv [i], "-Am") ||
			 !strcmp (argv [i], "-ba") || !strcmp (argv [i], "-bm") ||
			 !strcmp (argv [i], "-n"))
			continue;
		else if (!strncmp (argv [i], "-B", 2))
		{
			char const *enc = args (argc, argv, i, "Encoding expected after -B");
			if (strcmp (enc, "7BIT") && strcmp (enc, "8BITMIME"))
				error ("Invalid encoding '%s'", enc);
		}
		else if (!strcmp (argv [i], "-v"))
			log_level++;
		else if (!strcmp (argv [i], "-d0.1"))
		{
			printf ("mda version %u\n", VERSION);
			return EXIT_SUCCESS;
		}
		else if (!strncmp (argv [i], "-X", 2))
			TLOG = args (argc, argv, i, "File name expected after -X");
		else if (!strncmp (argv [i], "-o", 2) || !strncmp (argv [i], "-O", 2))
		{
			char name;
			Str value;
			bool lng = argv [i][1] == 'O';
			argo (lng,
				 args (argc, argv, i, "Option name and value expected after -o/-O"),
				 name, value);
			switch (name)
			{
				case 'e':
					new_error_mode = estrdup (value);
					break;
				case 'i':
					interactive = argb (value);
					break;
			}
		}
		else if (!strcmp (argv [i], "--"))
		{
			i++;
			break;
		}
		else if (argv [i][0] != '-')
			break;
		else
			error ("Unknown option %s.", argv [i]);
	}

	verbose (1, "Started");
	if (new_error_mode)
		error_set_mode (new_error_mode);
	while (i < argc)
	{
		verbose (1, "Recipient: %s", argv [i]);
		recipient_add (argv [i]);
		i++;
	}

	if (!from)
		from = from_local_user ();
	if (strlen (from))
		mail_check (from = from_local_user (from));

	bool header_date = false, header_from = false, header_to = false,
		 header_subject = false;
	regex_t *is_header = regex ("^([^ ]+): ");
	regex_t *group_addr = regex ("^[ \t\r\n]*("
		 "[a-zA-Z0-9!#$%&'*+/=?^_`{|}~ -]|\"\"|\".*[^\\\\]\""
		 ")+[ \t\r\n]*:[ \t\r\n]*;?[ \t\r\n]*");
	regex_t *email_addr = regex ("^ *([^,<]*<([^ ]+)>|([^ ,]+)) *[;,]? *");

	Str headers, line;
	bool contd = false;
	input.readline (line);
	if (line.length && !line.find (0) && regex_match (is_header, line))
	{
		Str header, header_name;
		size_t headers_last = 0;;
		header = line;
		headers += line;
		headers += "\r\n";
		do
		{
			line = "";
			input.readline (line);
			contd = line.length && (line [0] == ' ' || line [0] == '\t');

			if ((!line.length || !contd)
				 && !strncasecmp (&headers [headers_last], "bcc:", 4))
				headers.trunc (headers_last);

			if (line.length)
			{
				if (!contd)
					headers_last = headers.length;
				headers += line;
				headers += "\r\n";
				if (contd)
				{
					header += line;
					continue;
				}
			}

			if (header.find (0))
				error ("Nul character found in the header");
			if (!regex_match (is_header, header, 2, nullptr, &header_name))
				error ("Invalid header '%s'", header.data);

			if (header_name == "date")
				header_date = true;
			if (header_name == "from")
				header_from = true;
			if ((header_name == "to" || header_name == "cc" || header_name == "bcc"))
				header_to = true;
			if (header_name == "subject")
				header_subject = true;

			if (rcpt_headers && (header_name == "to" ||
					 header_name == "cc" || header_name == "bcc"))
			{
				char *p = header;
				p += header_name.length + 1;
				Str addr, email1, email2;
				do
				{
					if (regex_match(group_addr, p, 1, &addr))
					{
						p += addr.length;
						continue;
					}

					if (!regex_match (email_addr, p, 4, &addr, nullptr, &email1, &email2))
						error ("Invalid address header '%s'\n", header.data);
					verbose (1, "Recipient (%s): %s", header_name.data,
						 email1.length ? email1.data : email2.data);
					recipient_add (email1.length ? email1 : email2);
					p += addr.length;
				}
				while (*p);
			}

			header = line;
		}
		while (line.length);
	}

	if (!recipients)
		error ("No recipients given");
	verbose (1, "Adding missing headers");
	if (!header_from)
	{
		char const *f = from_local_user (from);
		mail_check (f);
		verbose (1, "  From: %s", f);
		headers.printf ("From: %s%s<%s>\r\n", full_name.data,
			 full_name.length ? " " : "", f);
	}
	if (!rcpt_headers && !header_to)
	{
		headers += "To: ";
		i = 0;
		for (Recipient *r = recipients; r; r = r->next)
		{
			verbose (1, "  To: %s", r->data);
			headers.printf ("%s%s", i++ ? ",\r\n    " : "", r->data);
		}
		headers += "\r\n";
	}
	if (!header_date)
	{
		verbose (1, "  Date");
		headers.strftime ("Date: %a, %e %b %Y %T %z\r\n", time (nullptr));
	}
	if (!header_subject)
	{
		verbose (1, "  Subject");
		headers.printf ("Subject: %s\r\n", subject.data);
	}
	headers += "\r\n";

	SMTP smtp (RELAY ? RELAY : "127.0.0.1",
		 TLOG ? trans_log_open (TLOG, argc, argv) : -1);

	smtp.writef ("MAIL FROM:<%s>\r\n", from);
	smtp.reply ();
	for (Recipient *r = recipients; r; r = r->next)
	{
		smtp.writef ("RCPT TO:<%s>\r\n", r->data);
		smtp.reply ();
	}

	verbose (1, "Sending the message");
	smtp.writef ("DATA\r\n");
	smtp.reply (3);
	smtp.write (headers);
	input.check_eof = true;
	if (!line.length)
		input.readline (line);
data:
	line += "\r\n";
	if (interactive && line [0] == '.')
		smtp.write (".", 1);
	smtp.write (line);
	if (interactive || line != ".\r\n")
	{
		line = "";
		input.readline (line);
		if (!input.eof)
			goto data;
		smtp.writef (".\r\n");
	}
	smtp.reply ();

	verbose (1, "Closing connection");
	smtp.writef ("QUIT\r\n");
	smtp.reply ();
	return EXIT_SUCCESS;
}
