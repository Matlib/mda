/*
   mda.hh
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
*/

#ifndef _MDA_HH
#define _MDA_HH 1

#include "util.hh"

/*
  DESCRIPTION

  The mda.hh/cc is the main program.

  RECIPIENT HANDLING

  The Recipient structures are a single-linked chain that hold the email
  addresses of the recipients. It has two variables: next – the pointer to the
  next Recipient structure, and data which is the actual string containing the
  email address.

  There are two global variables, recipients and recipients_last, that hold
  the values of the first and last recipient structure respectively.
*/

struct Recipient
{
	Recipient *next;
	char data [0];
};

/*
  recipient_add (char *)
    Adds one more email address to the recipient list.
*/

void recipient_add (char const *);

/*
  MAIL ADDRESS FUNCTIONS

  mail_check (char *)
    Does a simple check of the correctness of the email address given in the
    argument. It only follows the dot-atom part of the RFC 5322 specification
    which needs a far simpler regular expression to process. The quoted email
    addresses are rare if ever used.
*/

void mail_check (char const *);

/*
  from_local_user (char *)
    Returns sender email address generated from calling user name and fully
    qualified local host name. The optional argument specifies the original
    address part that will be normalized if necessary.
*/

char *from_local_user (char const *base = nullptr);

/*
  CONFIGURATION AND UTILITIES

  config_read ()
    Opens and reads the configuration file given in the CONFIG_FILE
    preprocessor variable. It is /etc/mda.conf by default but it may be altered
    when compiling.
*/

void config_read ();

/*
  Configuration file parameters:

    relay         RELAY   the default mail relay server
    transmit log  TLOG    server IO log file
    domain        DOMAIN  default sender domain
*/

extern char const *config [];
#define RELAY      config [0]
#define TLOG       config [1]
#define DOMAIN     config [2]
#define ERROR_MODE config [3]

/*
  trans_log_open (char *, argc, argv)
    Opens the transmission log file and writes current date as well as all the
    command line paramers the program was launched with. Returns the file
    descriptor of the log file.
*/

int trans_log_open (char const *, int argc, char const *const *argv);

/*
  args (argc, argv, int, char *)
    Reads the command line value for the argument given. The value may be stored
    immediately after the option string (like -Xvalue) or in the next argument
    (like -X value). Exits with the given error message if there are no more
    command line arguments left.
*/

char const *args (int argc, char const *const *argv, int &argp,
	 char const *error_msg);

/*
  argo (bool is_long, char *parameter, char, Str)
    Processes the option stored in the parameter and saves its short name in
    the third argument. Option value is copied to the Str. The first argument
    tells whether -O (long) or -o (short) was given.
*/

void argo (bool is_long, char const *opt, char &name, Str &value);

/*
  argb (Str)
    Checks whether the text string represents true or false. The following
    values are recognized:
    • 1, t, true and empty string – true,
    • 0, f, false – false.
*/

bool argb (Str &value);


#endif
