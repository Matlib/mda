.Dd July 11, 2020
.Dt mda 1
.Sh NAME
.Nm mda
.Nd delivers mail to a predefined relay server
.Sh SYNOPSIS
.Nm mda
.Op Fl i
.Op Fl t
.Op Fl f Ar sender
.Op Fl F Ar sender-name
.Op Fl S Ar subject
.Op Fl G Ar gateway
.Op Fl v
.Op Fl d Ns Ar 0.1
.Op Fl X Ar log-file
.Op Ar recipient No ...
.Sh DESCRIPTION
.Pp
The
.Nm mda
(mail delivery agent) is a
.Xr sendmail 8
like program that reads a message from standard input and delivers it to a
mail relay. It also replaces all line endings with the carriage return and
newline characters as required by RFC 821. The
.Em Bcc:
header is removed from the message if there is one.
.Sh USAGE
.Pp
It accepts the following command like arguments:
.Bl -tag
.It Fl i
Normally the message is delivered as-is to the mail relay, which implies the
special meaning of dots at the beginning of a line. Line with a lone dot is
interpreted as end of data marker, and the leading dot is stripped from the
lines that start with one.
.Pp
When the
.Em
interactive mode
is turned on with the
.Fl i
option, the input is treated as raw data until there is nothing more to read
from standard input.
.It Fl t
Scans the message headers for recipient addresses found in the
.Em To: , Cc: ,
and
.Em Bcc
headers. If both
.Fl t
and command line recipients are specified, then the list is merged (this
differs between various sendmail/mail server implementations).
.It Fl ti
Turns both the interactive mode
.No ( Fl i )
and recipient scaning
.No ( Fl t ) .
.It Fl F Ar name
Sets the display name of the sender. The maximum length is 400 bytes.
.It Fl f Ar address
Sets the so-called return path. This is the email address where all error
messages go to. If this address is not set explicitly, then it is composed from
the local user and host name.
.Pp
If you do not want any error replies to be generated (therefore any
undeliverable messages are usually discarded) then this should be set to an
empty string.
.It Fl S Ar subject
This is an extention not found in the original sendmail. It sets the
.Em Subject:
header.
.It Fl G Ar gateway
Uses this email gateway (with host:port notation). The default is read from
the configuration file falling back to
.Ql 127.0.0.1:25
.It Fl X Ar log-file
Turns on transmission logging to this file. The current date and time, and all
command line arguments are written, followed by all data read from and written
to the mail relay.
.It Fl v
Enables the verbose mode when some extra information regarding the operation of
the program is written to standard error.
.It Fl d Ns Ar 0.1
The
.Fl d
option sets the debugging levels in the original sendmail. The level
.Ar 0.1
is the only supported and causes the program to print its version number and
exit.
.El
.Pp
The following options are accepted:
.Bl -tag
.It Fl O Ns Ar ErrorMode Ns = Ns Em mode , Fl o Ns Ar e Ns Em mode
Set error mode. Mode
.Em p
(the default) causes errors to be printed to the console. Mode
.Em q
disables printing. Modes
.Em m , Em w No and Em e
cause the error to be written to
.Xr syslog 3
MAIL facility with priority ERROR.
In the original Sendmail they enable either sending bounce mail back or
.Xr write 1
the error message to the sender.
.It Fl O Ns Ar IgnoreDots Ns = Ns Em t/f , Fl o Ns Ar i Ns Em t/f
Enable or disable the interactive mode. See the description of the
.Fl i
option.
.El
.Pp
The following arguments are accepted and ignored to retain compatibility with
sendmail:
.Fl A Ns c , Fl Am , B , ba , bm , n .
.Sh THE CONFIGURATION FILE
The configuration file
CONFIG_FILE
contains the default configuration. The are two configuration parameters that
can be set:
.Bl -tag
.It Va relay No = Ar hostname:port
This parameter sets the default relay server that all mail will be delivered
to. It is the equivalent of the
.Fl G
command line option.
.It Va domain No = Ar sender-domain
Sets the default sender domain name for addresses that have only the local part,
that is when there is no
.Sy @
character followed by a domain name. The default value is local host's FQDN.
.It Va error mode No = Ar mode
Sets the initial error reporting mode (see the description of
.Fl O Ns Ar ErrorMode ) .
.It Va transmit log No = Pa log-file
Enables transmission logging to this file. It is the same as the
.Fl X
command line argument.
.El
.Sh EXAMPLES
.Pp
Send a simple message to multiple recipients:
.Bd -literal -offset 4m
( echo -n 'To: <alice@bobs.company>
Bcc: <archive@bobs.company>
Subject: New files '; date; echo; find . -mtime -1 ) | mda -ti
.Ed
.Pp
Send a text file setting the sender name as well as the subject. The return path
is empty inhibiting automatic replies.
.Bd -literal -offset 4m
mda -i -F 'Feliĉa Co.' -f '' -S 'Mangó Sales' < mango_report
.Ed
.Sh SEE ALSO
.Xr sendmail 8
.Sh AUTHORS
.An Matlib Ad matlib@matlibhax.com
