/*
   smtp.cc
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
*/

#include "smtp.hh"
#include "util.hh"

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <ctype.h>
#include <sys/socket.h>
#include <netdb.h>

SMTP::SMTP (char const *const gateway, int const trans_log)
{
	int e;
	char *host, *port;
	Str server_name (gateway);
	struct addrinfo *ai, *aii, hints;

	name = gateway;
	log = trans_log;

	host = server_name;
	if ((port = strrchr (host, ':')) && port > host && *(port - 1) != ':')
		*(port++) = 0;
	else
		port = (char *) "smtp";

	verbose (1, "Connecting to %s port %s", host, port);
	memset (&hints, 0, sizeof (hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	if ((e = getaddrinfo (host, port, &hints, &ai)))
		error ("Cannot get remote host address: %s", gai_strerror (e));
	Str sock_errors;
	char rhost [256], rport [128];
	for (aii = ai; aii; aii = aii -> ai_next)
	{
		if (getnameinfo (ai->ai_addr, ai->ai_addrlen, rhost, sizeof (rhost),
			 rport, sizeof (rport), NI_NUMERICHOST | NI_NUMERICSERV))
		{
			strcpy (rhost, "(unknown)");
			rport [0] = 0;
		}

		if (((fd =
			 socket (ai->ai_family, ai->ai_socktype, ai->ai_protocol)) != -1)
			 && !connect (fd, ai->ai_addr, ai->ai_addrlen))
		{
			freeaddrinfo (ai);
			verbose (1, "Connected to %s port %s", rhost, rport);
			reply ();
			writef ("EHLO %s\r\n", host_fqdn ());
			reply ();
			return;
		}

		e = errno;
		verbose (1, "  %s port %s: %s", rhost, rport, strerror (errno));
		sock_errors.printf ("\n%s:%s: %s", rhost, rport, strerror (errno));
		continue;
	}

	error ("Cannot connect to remote server:%s", (char *) sock_errors);
}

unsigned SMTP::reply (unsigned const expected)
{
	Str line, text;
	unsigned rc;
	do
	{
		line = "";
		readline (line);
		if (line.length < 4 || !isdigit (line [0]) || !isdigit (line [1]) ||
			!isdigit (line [2]) || (line [3] != ' ' && line [3] != '-'))
			error ("Invalid reply from server: %.50s", line.data);
		if (line.find (0))
			error ("Nul character in server reply %.50s...", line.data);
		if (text.length)
			text += "\n";
		text += &line [4];
	}
	while (line [3] == '-');

	rc = strtol (line, nullptr, 10);
	if (rc / 100 > 3)
		error ("Server error %u:\n%s", rc, text.data);
	if (rc / 100 != expected)
		error ("Unexpected server reply %u:\n%s\n", rc, text.data);
	return rc;
}
