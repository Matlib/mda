/*
   smtp.hh
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
*/

#ifndef _SMTP_HH
#define _SMTP_HH 1

#include "util.hh"

/*
  DESCRIPTION

  The smtp.hh/cc source files implement the SMTP protocol handling object.

  SMTP PROTOCOL

  The SMTP object which is an extention to the FD object defined in util.hh
  imlements the subset of the RFC 5321 simple mail transport protocol functions
  that allow communication with the remote mail server.

  It does not have its own attributes.

  There is one constructor that takes the mail relay address as string and
  an optional transmission log file handle where all input and output is copied
  to.

  There is one own method defined:

    • reply (unsigned)
      Reads the reply from the server and returns the reply code. Takes one
      optional argument that tells the reply code series (default is 2) that is
      expected to be returned by the server. A different reply code causes an
      error.
*/

struct SMTP : FD
{
	SMTP (char const *gateway, int log = -1);

	unsigned reply (unsigned expected = 2);
};

#endif
