/*
   util.cc
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
*/

#include "util.hh"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <time.h>
#include <ctype.h>
#include <sys/socket.h>
#include <netdb.h>
#include <syslog.h>

unsigned error_mode = 'p';

void error (char const *const format, ...)
{
	char msg [1024];
	memset (msg, 0, sizeof (msg));
	va_list ap;
	va_start (ap, format);
	vsnprintf (msg, sizeof (msg) - 1, format, ap);
	va_end (ap);

	if (error_mode == 'p')
	{
		fputs (msg, stderr);
		fputc ('\n', stderr);
	}

	else if (error_mode != 'q')
	{
		openlog ("mda", LOG_CONS | LOG_PID, LOG_MAIL);
		syslog (LOG_ERR, "%s", msg);
	}

	exit (error_mode == 'e' ? EXIT_SUCCESS : EXIT_FAILURE);
}

void error_set_mode (char const *const mode)
{
	if (strlen (mode) != 1 || (mode [0] != 'p' && mode [0] != 'q' &&
		 mode [0] != 'm' && mode [0] != 'w' && mode [0] != 'e'))
		error ("Invalid error mode '%s'", mode);
	error_mode = mode [0];
}

unsigned log_level = 0;

void verbose (unsigned const level, char const *const format, ...)
{
	if (log_level < level)
		return;
	char date [128];
	va_list ap;
	time_t t = time (nullptr);
	strftime (date, sizeof (date), "%F %H:%M:%S", localtime (&t));
	fprintf (stderr, "%s %u ", date, getpid ());
	va_start (ap, format);
	vfprintf (stderr, format, ap);
	va_end (ap);
	fputc ('\n', stderr);
}

void *emalloc (size_t const s)
{
	void *m = malloc (s);
	if (!m)
		error ("Cannot allocate %llu bytes of memory: %s", (unsigned long long) s,
			 strerror (errno));
	memset (m, 0, s);
	return m;
}

void erealloc (void **ptr, size_t const new_s)
{
	*ptr = realloc (*ptr, new_s);
	if (!*ptr)
		error ("Cannot allocate %llu bytes of memory: %s",
			 (unsigned long long) new_s, strerror (errno));
}

char *estrdup (char const *const s)
{
	char *copy = strdup (s);
	if (!copy)
		error ("Cannot allocate new string: %s", strerror (errno));
	return copy;
}

regex_t *regex (char const *const pattern)
{
	int e;
	regex_t *r = (regex_t *) emalloc (sizeof (regex_t));
	if ((e = regcomp (r, pattern, REG_EXTENDED)))
	{
		char err [1024];
		regerror (e, r, err, sizeof (err));
		error ("Cannot compile regex: %s", err);
	}
	return r;
}

bool regex_match (regex_t *r, char const *const s, unsigned vars, ...)
{
	unsigned i;
	va_list ap;
	Str *str;
	regmatch_t *m = (regmatch_t *) emalloc (sizeof (regmatch_t) * vars);

	if (regexec (r, s, vars, m, 0))
	{
		free (m);
		return false;
	}

	va_start (ap, vars);
	for (i = 0; i < vars; i++)
		if ((str = va_arg (ap, Str *)))
		{
			str->trunc (0);
			if (m [i].rm_so != -1 && m [i].rm_eo != -1)
				str->append (s + m [i].rm_so, m [i].rm_eo - m [i].rm_so);
		}

	free (m);
	return true;
}

bool regex_match (regex_t *const r, char const *const s)
{
	return !regexec (r, s, 0, nullptr, 0);
}

char *host_fqdn ()
{
	static char *hostname = nullptr;
	if (hostname)
		return hostname;

	hostname = (char *) emalloc (256);
	int e;
	struct addrinfo *ai, *aii, hints;

	if (gethostname (hostname, 256))
		error ("Cannot get host name: %s", strerror (errno));

	memset (&hints, 0, sizeof (hints));
	hints.ai_flags = AI_CANONNAME;
	if ((e = getaddrinfo (hostname, "smtp", &hints, &ai)))
		return hostname;

	for (aii = ai; aii; aii = aii -> ai_next)
	{
		if (aii->ai_canonname)
		{
			memset (hostname, 0, 256);
			strncpy (hostname, aii->ai_canonname, 255);
			break;
		}
	}

	return hostname;
}

Str::Str (size_t initial_length)
{
	size = initial_length;
	length = 0;
	data = (char *) emalloc (initial_length);
}

Str::Str (char const *const initial_value)
{
	size_t initial_length = strlen (initial_value);
	size = initial_length + 32;
	length = initial_length;
	data = (char *) emalloc (size);
	memcpy (data, initial_value, initial_length);
}

Str::~Str ()
{
	free (data);
}

void Str::trunc (size_t const new_length)
{
	if (new_length > 1048576)
		error ("String starting with '%.20s...' too long", data);
	if (new_length >= size)
	{
		size = new_length + 128;
		erealloc ((void **) &data, size);
	}
	length = new_length;
	data [length] = 0;
}

void Str::append (char const *const s, size_t const s_length)
{
	if (s_length > 1048576)
		error ("String starting with '%.20s...' too long", data);
	if (length + s_length >= size)
	{
		size += s_length + 128;
		if (size > 1048576)
			error ("String containing '%.20s...' too long", data);
		erealloc ((void **) &data, size);
	}
	memcpy (data + length, s, s_length);
	length += s_length;
	data [length] = 0;
}

void Str::printf (char const *const format, ...)
{
	va_list ap;
	size_t written, avail = size - length;
	va_start (ap, format);
	written = vsnprintf (data + length, avail, format, ap);
	va_end (ap);
	if (written < 0)
		error ("Print error: %s", strerror (errno));
	if (written < avail)
	{
		length += written;
		data [length] = 0;
		return;
	}

	size += written + 128;
	if (size > 1048576)
		error ("String containing '%.20s...' too long", data);
	erealloc ((void **) &data, size);
	avail = size - length;
	va_start (ap, format);
	written = vsnprintf (data + length, avail, format, ap);
	va_end (ap);
	if (written < 0 || written >= avail)
		error ("Print error: %s", strerror (errno));
	length += written;
	data [length] = 0;
}

void Str::strftime (char const *const format, time_t const t)
{
	char s [1024];
	::strftime (s, sizeof (s) - 1, format, gmtime (&t));
	*this += s;
}

void Str::base64 (char const *const src, size_t const src_length)
{
	static char const *const alphabet =
		 "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	trunc (0);
	append ("=?UTF-8?B?", 10);
	unsigned s = 2;
	unsigned c = 0;
	for (size_t p = 0; p < src_length; p++)
	{
		c = (c << 8) | (unsigned char) src [p];
		append (alphabet + (c >> s), 1);
		c = c & (0xff >> (8 - s));
		s += 2;
		if (s == 8)
		{
			append (alphabet + c, 1);
			s = 2;
			c = 0;
		}
	}
	if (s > 2)
	{
		append (alphabet + (c << (8 - s)), 1);
		append ("==", 4 - s / 2);
	}
	append ("?=", 2);
}

bool operator == (Str const &str, char const *s)
{
	if (str.length != strlen (s))
		return false;
	char *p = str.data;
	while (*s)
	{
		if (tolower (*s) != tolower (*p))
			return false;
		s++;
		p++;
	}
	return true;
}

bool operator != (Str const &str, char const *s)
{
	return !(str == s);
}

void FD::write (char const *data, size_t length)
{
	ssize_t wr;
	if (log != -1)
		::write (log, data, length);
	while (length)
	{
		wr = ::write (fd, data, length);
		if (wr == -1)
		{
			if (errno == EINTR)
				continue;
			error ("Cannot write to %s: %s", name, strerror (errno));
		}
		data += wr;
		length -= wr;
	}
}

void FD::writef (char const *const format, ...)
{
	va_list ap;
	size_t written;
	char *buffer = (char *) emalloc (1024);
	va_start (ap, format);
	written = vsnprintf (buffer, 1024, format, ap);
	va_end (ap);
	if (written < 0)
		error ("Cannot compose message: %s", strerror (errno));
	if (written >= 1024)
	{
		erealloc ((void **) &buffer, written + 1);
		va_start (ap, format);
		vsnprintf (buffer, written + 1, format, ap);
		va_end (ap);
	}
	write (buffer, written);
	free (buffer);
}

void FD::read ()
{
	if (bp)
	{
		memmove (buffer, buffer + bp, bl - bp);
		bl -= bp;
		bp = 0;
	}
	if (bl == sizeof (buffer))
		return;
	ssize_t r = ::read (fd, buffer + bl, sizeof (buffer) - bl);
	if (r == -1)
		error ("Cannot read data from %s: %s", name, strerror (errno));
	if (!r)
	{
		if (!check_eof)
			error ("No more data to read from %s", name);
		eof = true;
	}
	else
	{
		eof = false;
		if (log != -1)
			::write (log, buffer + bl, r);
	}
	bl += r;
}

void FD::readline (Str &str)
{
	if (!length ())
		read ();
	if (eof)
		return;

	char *p, *start = buffer + bp;
	while (!(p = (char *) memchr (start, '\n', length ())))
	{
		str.append (start, length ());
		bp = bl;
		read ();
		start = buffer;
	}

	size_t diff = (size_t) p - (size_t) start;
	str.append (start, diff > 0 && *(p - 1) == '\r' ? diff - 1 : diff);
	bp += diff + 1;
}
