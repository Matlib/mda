/*
   util.hh
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
*/

#ifndef _UTIL_HH
#define _UTIL_HH 1

#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <regex.h>

/*
  DESCRIPTION

  The util.hh/cc implement some utility macros and functions like error
  handling or memory allocation. It also provides the dynamic buffer object Str
  and the IO reading and writing object FD.

  MACROS

  noreturn
    Adds attribute "noreturn" which tells the compiler that the function never
    returns.
*/

#define noreturn __attribute__ ((noreturn))

/*
  str (text)
    Converts the bare text to a string.
*/

#define str(__TEXT__) _str (__TEXT__)
#define _str(__TEXT__) #__TEXT__

/*
  alen (array)
    Computes the dimension of the array.
*/
#define alen(__ARRAY__) \
         ((unsigned) (sizeof (__ARRAY__) / sizeof ((__ARRAY__) [0])))

/*
  ERROR HANDLING

  error_set_mode
    Adjusts the way errors are handled:
    • p – print
    • q – only return exit code
    • m, w – write to syslog
    • e – write to syslog and always return EXIT_SUCCESS
*/

void error_set_mode (char const *);

/*
  error (format, ...)
    Prints an error message and exits the program with EXIT_FAILURE. The
    message is constructed just like printf(3) does it.
*/


noreturn void error (char const *format, ...);

extern unsigned log_level;
void verbose (unsigned level, char const *format, ...);

/*
  MEMORY ALLOCATION

  emalloc (size)
  erealloc (ptr, size)
  estrdup (char *)
    Similar to malloc(3), realloc(3) and strdup(3), but they also check for
    errors. Additionally, emalloc() also wipes the memory.
*/

void *emalloc (size_t);
void erealloc (void **, size_t);
char *estrdup (char const *);

/*
  REGULAR EXPRESSIONS

  regex (char *)
    Allocates and compiles the extended regular expression given in the
    argument. Returns the regex_t * object.
*/


regex_t *regex (char const *);

/*
  regex_match (regex_t, char *, unsigned, Str *, Str *, ...)
  regex_match (regex_t, char *)
    Both funtions return true if the given string matches the regexp (note that
    this is the inverse what regexec(3) returns). The former also accepts
    additional arguments, the first one being the number of Str * structures
    that follow. The whole match and any capture groups will be stored in
    these. Each token may be skipped by passing nullptr.
*/

bool regex_match (regex_t *, char const *string, unsigned vars, ...);
bool regex_match (regex_t *, char const *);

/*
  UTILITY FUNCTIONS

  host_fqdn ()
    Returns this hosts fully qualified domain name if it can be determined.
*/

char *host_fqdn ();

/*
  STRINGS

  The Str object is a buffer of memory that contains arbitrary binary data.
  It has three attributes:

    • size (size_t) that stores the amount of memory allocated,
    • length (size_t) the actual length of the data stored,
    • data (char *) the data itself.

  The methods make sure the content is always ended with a zero byte (that does
  not count as its length).

  Str is constructed by giving an optional initial buffer size or by supplying
  a char * string to it.

    Str x;
    Str y ("Hello, world!");

  The following operators are defined:

    • casting to char * (returns the pointer to the content),
    • = copies the content of the right side Str or nul terminated char *,
    • += appends the content of the right side Str or char *,
    • == and != do case insensitive comparison to right side char *.

  The following methods are defined:

  trunc (size_t)
    Truncate the length of the Str to the given value.

  append (char *, size_t)
    Copy the data from the arguments to the end of the buffer.

  printf (char *, ...)
    Build the string from the printf(3) style arguments and append it to the
    end of this Str.

  strftime (char *, time_t)
    Similarly, append what strftime(3) call returns to the end of this Str.

  base64 (char *, size_t);
  base64 (char *);
    Converts the data given to base64 encoding and stores it in this Str. The
    previous content of the Str is discarded.

  find (char)
    Returns the char * pointer of the first occurrence of the given character
    in the Str, or nullptr if no such characters are found.
*/

struct Str
{
	size_t size;
	size_t length;
	char *data;

	Str (size_t initial_length = 128);
	Str (char const *const initial_value);
	~Str ();

	void trunc (size_t);
	void append (char const *, size_t);
	void printf (char const *format, ...);
	void strftime (char const *format, time_t);

	char *find (char const c)
		 { return (char *) memchr (data, c, length); }

	void base64 (char const *, size_t);
	void base64 (char const *const s) { base64 (s, strlen (s)); }

	operator char * () { return data; }
	Str &operator = (Str const &str) { length = 0;
		 if (str.length) append (str.data, str.length); return *this; }
	Str &operator = (char const *const s) { length = 0;
		 if (strlen (s)) append (s, strlen (s)); return *this; }
	Str &operator += (Str const &str)
		 { append (str.data, str.length); return *this; }
	Str &operator += (char const *const s)
		 { append (s, strlen (s)); return *this; }
};

bool operator == (Str const &, char const *);
bool operator != (Str const &, char const *);

/*
  FILE HANDLE

  The FD objects stores a file descriptor and its read-ahead buffer. It has
  the following attributes:

    • fd – the file handle,
    • name – the descriptive name of the object that is printed with errors,
    • buffer – the 10KB read-ahead buffer,
    • bp – current byte pointer in the buffer,
    • bl – the number of bytes stored in the buffer,
    • eof – indicates if there were no more data available for reading,
    • check_eof – controls whether EOF should be detected and stored in the
      eof attribute (true) or cause immediate error (false),
    • log – the optional transmission log file handle where all reads and
      writes are copied to; the value of -1 (default) disables logging.

  The following constructors are defined:

    • FD () – initializes a plain object with zeroes or nullptr,
    • FD (char *, int) – creates the object from the file handle in the second
      argument and gets the name from the first one.

  The following methods are defined:

  length ()
    Computes the number of bytes still available in the read-ahead buffer.

  write (char *, size_t)
  write (Str)
    Writes the data given to the handle.

  writef (char *, ...)
    Works like the printf(3) family of functions generating the content and
    writing it to the handle.

  read ()
    Reads some more data into the read-ahead buffer. Does not return anything.

  readline (Str)
    Stores a complete line – that is all bytes until a newline character is
    found – in the Str object supplied as the argument. The newline character
    is not included in the Str. The last carriage return character will also be
    removed if there is one.
*/

struct FD
{
	char const *name;
	int fd;
	char buffer [10240];
	unsigned bp, bl;
	bool eof, check_eof;
	int log;

	FD () : name(nullptr), fd(0), bp(0), bl(0), eof(false), check_eof(false),
		 log(-1) {}
	FD (char const *const name, int const fd) : name(name), fd(fd), bp(0),
		 bl(0), eof(false), check_eof(false), log(-1) {}
	~FD () { close (fd); }

	unsigned length () { return bl - bp; }
	void write (char const *, size_t);
	void write (Str &str) { write (str, str.length); }
	void writef (char const *format, ...);
	void read ();
	void readline (Str &);
};

#endif
